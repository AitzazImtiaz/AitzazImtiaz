![](https://github.com/AitzazImtiaz/Public-Images/blob/main/IMG-20220801-180318.jpg) I AM AITZAZ IMTIAZ 
# 💫 About Me:
Hello World,<br>I am Aitzaz Imtiaz, a tech-obsessed person, coding for fun. I am a 'future-American' and I love crafting new things.
![](https://github.com/AitzazImtiaz/Public-Images/blob/main/IMG-20220801-184654.jpg)

# 💻 Tech Stack:
![C++](https://img.shields.io/badge/c++-%2300599C.svg?style=for-the-badge&logo=c%2B%2B&logoColor=white) ![.Net](https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white) ![Canva](https://img.shields.io/badge/Canva-%2300C4CC.svg?style=for-the-badge&logo=Canva&logoColor=white) ![NumPy](https://img.shields.io/badge/numpy-%23013243.svg?style=for-the-badge&logo=numpy&logoColor=white) ![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white) ![SciPy](https://img.shields.io/badge/SciPy-%230C55A5.svg?style=for-the-badge&logo=scipy&logoColor=%white) ![CMake](https://img.shields.io/badge/CMake-%23008FBA.svg?style=for-the-badge&logo=cmake&logoColor=white) ![C](https://img.shields.io/badge/c-%2300599C.svg?style=for-the-badge&logo=c&logoColor=white) ![Markdown](https://img.shields.io/badge/markdown-%23000000.svg?style=for-the-badge&logo=markdown&logoColor=white) ![Cloudflare](https://img.shields.io/badge/Cloudflare-F38020?style=for-the-badge&logo=Cloudflare&logoColor=white) ![Heroku](https://img.shields.io/badge/heroku-%23430098.svg?style=for-the-badge&logo=heroku&logoColor=white) ![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Lua](https://img.shields.io/badge/lua-%232C2D72.svg?style=for-the-badge&logo=lua&logoColor=white) ![LaTeX](https://img.shields.io/badge/latex-%23008080.svg?style=for-the-badge&logo=latex&logoColor=white)
# 📊 GitHub Stats:
![](https://github-readme-stats.vercel.app/api?username=AitzazImtiaz&theme=dark&hide_border=false&include_all_commits=true&count_private=true)<br/>
![](https://github-readme-streak-stats.herokuapp.com/?user=AitzazImtiaz&theme=dark&hide_border=false)<br/>
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=AitzazImtiaz&langs_count=10)](https://github.com/anuraghazra/github-readme-stats)

## 📚 Libraries
The only part of my entire GitHub that makes me feel special is the small and medium sized libraries I have made in honour to my inspirations. Ya don't know, these vibes make me feel high!

![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=AitzazImtiaz&repo=Germain) ![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=AitzazImtiaz&repo=Hypatia) ![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=AitzazImtiaz&repo=ImtiazGermain) ![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=AitzazImtiaz&repo=Monnink) 

## 🏆 GitHub Trophies
![](https://github-profile-trophy.vercel.app/?username=AitzazImtiaz&theme=radical&no-frame=false&no-bg=false&margin-w=4)

### ✍️ Random Dev Quote
![](https://quotes-github-readme.vercel.app/api?type=horizontal&theme=radical)

## 💼 Hire me?
Sorry, [Ariana Grande](https://en.wikipedia.org/wiki/Ariana_Grande) hired me already, but the point is, why the hell you even considered me? I work for..., technically humanity, that is why, I love staying jobless, unless my salary starts  at $80k :)

# Finally
Watch THE LAST GUEST, I am in love with Matt aka [ObliviousHD](https://oblivioushd.fandom.com/wiki/ObliviousHD)'s creation, I love it all, He is a genius!
![wp4768292](https://user-images.githubusercontent.com/100691531/182167576-109476e7-b513-449c-9d0a-aba31f6c8ac3.jpg)

